import 'package:flutter/material.dart';
import 'package:movie_zapp/data_provider/provider_movies.dart';
import 'package:movie_zapp/screens/screen_detailed_movie.dart';
import 'package:movie_zapp/screens/screen_downloads.dart';
import 'package:movie_zapp/screens/screen_home.dart';
import 'package:provider/provider.dart';

void main() => runApp(MovieZapp());

class MovieZapp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [ChangeNotifierProvider.value(value: ProviderMovies())],
      child: MaterialApp(
        title: 'MoviZapp',
        theme: ThemeData(
          primarySwatch: Colors.red,
          fontFamily: 'montserrat',
        ),
        routes: {
          '/': (ctx) => ScreenHome(),
          ScreenDownloads.routeName: (ctx) => ScreenDownloads(),
          ScreenDetailedMovie.routeName:(ctx)=>ScreenDetailedMovie(),
        },
      ),
    );
  }
}
