import 'package:flutter/material.dart';
import 'package:movie_zapp/data_provider/provider_movies.dart';
import 'package:movie_zapp/widgets/ui_item_download.dart';
import 'package:provider/provider.dart';

class ScreenDownloads extends StatefulWidget {
  static final routeName = '/downloads';

  @override
  _ScreenDownloadsState createState() => _ScreenDownloadsState();
}

class _ScreenDownloadsState extends State<ScreenDownloads> {
  var primaryColor;

  @override
  Widget build(BuildContext context) {
    final movieList = Provider.of<ProviderMovies>(context).MovieList;
    primaryColor = Theme.of(context).primaryColor;
    return Scaffold(
      appBar: _downloadsAppBar,
      body: ListView.builder(
          itemCount: 10,
          itemBuilder: (ctx, index) {
            return UiItemDownload(movieList[index].img, movieList[index].by,
                movieList[index].link1);
          }),
    );
  }

  AppBar get _downloadsAppBar {
    return AppBar(
      title: Text(
        'Downloads',
        style: TextStyle(
          color: primaryColor,
          fontWeight: FontWeight.bold,
        ),
      ),
      iconTheme: IconThemeData(color: primaryColor),
      backgroundColor: Colors.white,
    );
  }
}
