import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:movie_zapp/data_provider/provider_movies.dart';
import 'package:movie_zapp/screens/screen_downloads.dart';
import 'package:movie_zapp/widgets/cateroty_movies.dart';
import 'package:movie_zapp/widgets/drawer_home.dart';
import 'package:movie_zapp/widgets/ui_item_image_slider.dart';
import 'package:provider/provider.dart';

class ScreenHome extends StatefulWidget {
  static final routeName = '/';

  @override
  _ScreenHomeState createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  var primaryColor;
  final imageSliderController = PageController(
    initialPage: 1,
  );

  @override
  Widget build(BuildContext context) {
    primaryColor = Theme.of(context).primaryColor;
    final movieDB = Provider.of<ProviderMovies>(context, listen: true);
    return Scaffold(
      appBar: _homeAppBar,
      drawer: DrawerHome(),
      body: movieDB.isFetchinng
          ? Center(child: CircularProgressIndicator())
          : SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  CarouselSlider.builder(
                      autoPlay: true,
                      aspectRatio: 2.0,
                      itemCount: 3,
                      itemBuilder: (ctx, index) {
                        return UiItemImageSlider(movieDB.MovieList[index].img);
                      }),
                  CategoryMovies('Latest'),
                  CategoryMovies('Top Rated'),
                  CategoryMovies('Malayalam')
                ],
              ),
            ),
    );
  }

  AppBar get _homeAppBar {
    return AppBar(
      title: Text(
        'MovieZapp',
        style: TextStyle(
          color: primaryColor,
          fontWeight: FontWeight.bold,
        ),
      ),
      iconTheme: IconThemeData(color: primaryColor),
      backgroundColor: Colors.white,
      actions: <Widget>[
        IconButton(
          icon: Icon(
            Icons.search,
            color: Colors.black,
          ),
          onPressed: () {},
        ),
        IconButton(
          icon: Icon(
            Icons.arrow_downward,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pushNamed(ScreenDownloads.routeName);
          },
        ),
        IconButton(
          icon: Icon(
            Icons.share,
            color: Colors.black,
          ),
          onPressed: () {},
        )
      ],
    );
  }
}
