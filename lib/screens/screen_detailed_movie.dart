import 'package:flutter/material.dart';
import 'package:movie_zapp/data_provider/provider_movies.dart';
import 'package:provider/provider.dart';

class ScreenDetailedMovie extends StatelessWidget {
  static final routeName='/detailed_movie';

  @override
  Widget build(BuildContext context) {
    final id=ModalRoute.of(context).settings.arguments as String;
    var movieDetails=Provider.of<ProviderMovies>(context).getMovieByID(id);
    return Scaffold(body: Text(movieDetails.by),);
  }
}
