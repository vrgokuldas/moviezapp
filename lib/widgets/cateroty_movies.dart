import 'package:flutter/material.dart';
import 'package:movie_zapp/data_provider/provider_movies.dart';
import 'package:provider/provider.dart';
import 'ui_item_movie.dart';

class CategoryMovies extends StatelessWidget {
  final title;

  CategoryMovies(this.title);

  @override
  Widget build(BuildContext context) {
    final primaryColor = Theme.of(context).primaryColor;
    final movieDB = Provider.of<ProviderMovies>(context);
    final itemCount = movieDB.moviesCount;
    final movieList = movieDB.MovieList;
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          padding: EdgeInsets.all(15),
          width: double.infinity,
          child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _drawLine,
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        title,
                        style: TextStyle(
                          color: primaryColor,
                          fontWeight: FontWeight.w700,
                          fontSize: 20,
                        ),
                      )
                    ],
                  ),
                  Text(
                    'MORE',
                    style: TextStyle(
                      color: primaryColor,
                      fontWeight: FontWeight.w700,
                      fontSize: 14,
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
        Container(
          height: 230,
          margin: EdgeInsets.symmetric(horizontal: 10),
          child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: itemCount,
            itemBuilder: (ctx, i) {
              return UiItemMovie(
                id: movieList[i].id ,
                movieName: movieList[i].by,
                ratings: movieList[i].rating,
                posterUrl: movieList[i].img,
                type: movieList[i].dvd,
              );
            },
          ),
        )
      ],
    );
  }

  Widget get _drawLine {
    return Container(
      width: 5,
      height: 30,
      color: Colors.red,
    );
  }
}
