import 'package:flutter/material.dart';

class UiItemImageSlider extends StatelessWidget {
  final imageUrl;

  UiItemImageSlider(this.imageUrl);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Stack(
        children: <Widget>[
          Container(
            width: MediaQuery.of(context).size.width,
            child: Image.network(
              imageUrl,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            color: Colors.black38,
          ),
          Positioned(
            right: 10,
            bottom: 10,
            child: Text(
              "Coming Soon",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.w700,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
    );
  }
}
