import 'package:flutter/material.dart';
import 'package:movie_zapp/widgets/progressbar.dart';

enum DownloadProgress { isPaused, isDownloading, isFinishedDownloading }

class UiItemDownload extends StatefulWidget {
  final imageUrl;
  final movieName;
  final downloadUrl;

  UiItemDownload(this.imageUrl, this.movieName, this.downloadUrl);

  @override
  _UiItemDownloadState createState() => _UiItemDownloadState();
}

class _UiItemDownloadState extends State<UiItemDownload> {
  var progress = DownloadProgress.isPaused;

  @override
  Widget build(BuildContext context) {
    return Dismissible(
      direction: DismissDirection.endToStart,
      key: Key(widget.movieName),
      background: Container(
        color: Colors.red,
        child: Icon(
          Icons.delete,
          size: 40,
          color: Colors.white,
        ),
        alignment: Alignment.centerRight,
        padding: EdgeInsets.only(right: 10),
      ),
      child: Card(
        child: ListTile(
          leading: CircleAvatar(
            radius: 20,
            backgroundImage: NetworkImage(widget.imageUrl),
          ),
          title: Text(widget.movieName),
          subtitle: _progressBar(30),
          trailing: _trailingIcon(),
        ),
      ),
    );
  }

  Widget _progressBar(int currentValue) {
    return Container(
      height: 15,
      child: FAProgressBar(
        currentValue: 200,
        maxValue: 500,
        displayText: '/500 MB',
        direction: Axis.horizontal,
        backgroundColor: Colors.transparent,
        progressColor: Colors.red,
        borderColor: Colors.grey,
      ),
    );
  }

  Widget _trailingIcon() {
    Widget button;
    switch (progress) {
      case DownloadProgress.isDownloading:
        button = IconButton(
          icon: Icon(Icons.pause),
          onPressed: () {
            setState(() {
              progress = DownloadProgress.isPaused;
            });
          },
        );
        break;
      case DownloadProgress.isPaused:
        button = IconButton(
          icon: Icon(Icons.arrow_downward),
          onPressed: () {
            setState(() {
              progress = DownloadProgress.isDownloading;
            });
          },
        );
        break;
      case DownloadProgress.isFinishedDownloading:
        button = IconButton(
          icon: Icon(Icons.play_arrow),
          onPressed: () {
            setState(() {
              print("play video");
            });
          },
        );
        break;
    }
    return button;
  }
}
