import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:movie_zapp/screens/screen_detailed_movie.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class UiItemMovie extends StatelessWidget {
  final movieName;
  final ratings;
  final posterUrl;
  final type;
  final id;

  UiItemMovie(
      {this.id, this.movieName, this.ratings, this.posterUrl, this.type});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        width: 135,
        child: ClipRRect(
          borderRadius: BorderRadius.circular(15),
          child: Card(
            child: Stack(
              children: <Widget>[
                Center(
                  child: CircularProgressIndicator(
                    strokeWidth: 1,
                  ),
                ),
                InkWell(
                  onTap: (){
                    Navigator.of(context).pushNamed(ScreenDetailedMovie.routeName,arguments: id);
                  },
                  child: Image.network(
                    posterUrl,
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: double.infinity,
                  ),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 7,
                        vertical: 5,
                      ),
                      child: Text(
                        type,
                        style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.w700,
                            fontSize: 12),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.red,
                          borderRadius: BorderRadius.circular(5)),
                    ),
                    Container(
                      color: Colors.black54,
                      width: double.infinity,
                      padding: const EdgeInsets.symmetric(
                          horizontal: 5, vertical: 10),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            movieName,
                            maxLines: 1,
                            style: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.bold),
                          ),
                          SmoothStarRating(
                              allowHalfRating: true,
                              starCount: 5,
                              size: 20.0,
                              rating: 5,
                              filledIconData: Icons.star,
                              halfFilledIconData: Icons.star_half,
                              color: Colors.amber,
                              borderColor: Colors.amber,
                              spacing: 0.0)
                        ],
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  bool isNumeric(String s) {
    if (s == null) {
      return false;
    }
    return double.tryParse(s) != null;
  }
}
