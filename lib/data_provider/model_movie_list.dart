class ModelMovieList {
  List<ModelMovie> movieList;

  ModelMovieList(this.movieList);

  factory ModelMovieList.fromJson(List<dynamic> jsonString) {
    List<ModelMovie> _movieList = new List<ModelMovie>();
    _movieList = jsonString.map((item) => ModelMovie.fromJson(item)).toList();
    return ModelMovieList(_movieList);
  }
}

class ModelMovie {
  String _id;
  String _by;
  String _poste;
  String _time;
  String _act;
  String _year;
  String _dvd;
  String _rating;
  String _plot;
  String _genre;
  String _img;
  String _size;
  String _scr1;
  String _scr2;
  String _scr3;
  String _scr4;
  String _text1;
  String _text2;
  String _text3;
  String _alt;
  String _link1;
  String _link2;
  String _link3;
  String _server;
  String _glink1;
  String _glink2;
  String _glink3;
  String _m2;
  String _dub;
  String _hevc;
  String _imdb;

  ModelMovie(
      {String id,
      String by,
      String poste,
      String time,
      String act,
      String year,
      String dvd,
      String rating,
      String plot,
      String genre,
      String img,
      String size,
      String scr1,
      String scr2,
      String scr3,
      String scr4,
      String text1,
      String text2,
      String text3,
      String alt,
      String link1,
      String link2,
      String link3,
      String server,
      String glink1,
      String glink2,
      String glink3,
      String m2,
      String dub,
      String hevc,
      String imdb}) {
    this._id = id;
    this._by = by;
    this._poste = poste;
    this._time = time;
    this._act = act;
    this._year = year;
    this._dvd = dvd;
    this._rating = rating;
    this._plot = plot;
    this._genre = genre;
    this._img = img;
    this._size = size;
    this._scr1 = scr1;
    this._scr2 = scr2;
    this._scr3 = scr3;
    this._scr4 = scr4;
    this._text1 = text1;
    this._text2 = text2;
    this._text3 = text3;
    this._alt = alt;
    this._link1 = link1;
    this._link2 = link2;
    this._link3 = link3;
    this._server = server;
    this._glink1 = glink1;
    this._glink2 = glink2;
    this._glink3 = glink3;
    this._m2 = m2;
    this._dub = dub;
    this._hevc = hevc;
    this._imdb = imdb;
  }

  String get id => _id;

  set id(String id) => _id = id;

  String get by => _by;

  set by(String by) => _by = by;

  String get poste => _poste;

  set poste(String poste) => _poste = poste;

  String get time => _time;

  set time(String time) => _time = time;

  String get act => _act;

  set act(String act) => _act = act;

  String get year => _year;

  set year(String year) => _year = year;

  String get dvd => _dvd;

  set dvd(String dvd) => _dvd = dvd;

  String get rating => _rating;

  set rating(String rating) => _rating = rating;

  String get plot => _plot;

  set plot(String plot) => _plot = plot;

  String get genre => _genre;

  set genre(String genre) => _genre = genre;

  String get img => _img;

  set img(String img) => _img = img;

  String get size => _size;

  set size(String size) => _size = size;

  String get scr1 => _scr1;

  set scr1(String scr1) => _scr1 = scr1;

  String get scr2 => _scr2;

  set scr2(String scr2) => _scr2 = scr2;

  String get scr3 => _scr3;

  set scr3(String scr3) => _scr3 = scr3;

  String get scr4 => _scr4;

  set scr4(String scr4) => _scr4 = scr4;

  String get text1 => _text1;

  set text1(String text1) => _text1 = text1;

  String get text2 => _text2;

  set text2(String text2) => _text2 = text2;

  String get text3 => _text3;

  set text3(String text3) => _text3 = text3;

  String get alt => _alt;

  set alt(String alt) => _alt = alt;

  String get link1 => _link1;

  set link1(String link1) => _link1 = link1;

  String get link2 => _link2;

  set link2(String link2) => _link2 = link2;

  String get link3 => _link3;

  set link3(String link3) => _link3 = link3;

  String get server => _server;

  set server(String server) => _server = server;

  String get glink1 => _glink1;

  set glink1(String glink1) => _glink1 = glink1;

  String get glink2 => _glink2;

  set glink2(String glink2) => _glink2 = glink2;

  String get glink3 => _glink3;

  set glink3(String glink3) => _glink3 = glink3;

  String get m2 => _m2;

  set m2(String m2) => _m2 = m2;

  String get dub => _dub;

  set dub(String dub) => _dub = dub;

  String get hevc => _hevc;

  set hevc(String hevc) => _hevc = hevc;

  String get imdb => _imdb;

  set imdb(String imdb) => _imdb = imdb;

  ModelMovie.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _by = json['by'];
    _poste = json['poste'];
    _time = json['time'];
    _act = json['act'];
    _year = json['year'];
    _dvd = json['dvd'];
    _rating = json['rating'];
    _plot = json['plot'];
    _genre = json['genre'];
    _img = json['img'];
    _size = json['size'];
    _scr1 = json['scr1'];
    _scr2 = json['scr2'];
    _scr3 = json['scr3'];
    _scr4 = json['scr4'];
    _text1 = json['text1'];
    _text2 = json['text2'];
    _text3 = json['text3'];
    _alt = json['alt'];
    _link1 = json['link1'];
    _link2 = json['link2'];
    _link3 = json['link3'];
    _server = json['server'];
    _glink1 = json['glink1'];
    _glink2 = json['glink2'];
    _glink3 = json['glink3'];
    _m2 = json['m2'];
    _dub = json['dub'];
    _hevc = json['hevc'];
    _imdb = json['imdb'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['by'] = this._by;
    data['poste'] = this._poste;
    data['time'] = this._time;
    data['act'] = this._act;
    data['year'] = this._year;
    data['dvd'] = this._dvd;
    data['rating'] = this._rating;
    data['plot'] = this._plot;
    data['genre'] = this._genre;
    data['img'] = this._img;
    data['size'] = this._size;
    data['scr1'] = this._scr1;
    data['scr2'] = this._scr2;
    data['scr3'] = this._scr3;
    data['scr4'] = this._scr4;
    data['text1'] = this._text1;
    data['text2'] = this._text2;
    data['text3'] = this._text3;
    data['alt'] = this._alt;
    data['link1'] = this._link1;
    data['link2'] = this._link2;
    data['link3'] = this._link3;
    data['server'] = this._server;
    data['glink1'] = this._glink1;
    data['glink2'] = this._glink2;
    data['glink3'] = this._glink3;
    data['m2'] = this._m2;
    data['dub'] = this._dub;
    data['hevc'] = this._hevc;
    data['imdb'] = this._imdb;
    return data;
  }
}
