import 'dart:convert';
import 'dart:math';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:movie_zapp/data_provider/model_movie_list.dart';

class ProviderMovies with ChangeNotifier {

  List<ModelMovie> _movieList=new List<ModelMovie>();

  List<ModelMovie> get MovieList => [...shuffle(_movieList)];

  bool isFetchinng=false;

  ProviderMovies()
  {
    fetchMovieList();
  }

  Future fetchMovieList() async
  {
    isFetchinng=true;
    Response response= await Dio().get<String>('https://mallumusic.tk/myapp/all.php?all');
    //print(response.data);
    _movieList=ModelMovieList.fromJson(json.decode(response.data.toString())).movieList;
    isFetchinng=false;
    notifyListeners();
  }

  int get moviesCount {
    return _movieList.length;
  }

  ModelMovie getMovieByID(String movieID) {
    return _movieList.firstWhere((movie) => movie.id == movieID);
  }

  List shuffle(List items) {
    var random = new Random();
    for (var i = items.length - 1; i > 0; i--) {
      var n = random.nextInt(i + 1);
      var temp = items[i];
      items[i] = items[n];
      items[n] = temp;
    }
    return items;
  }
}
